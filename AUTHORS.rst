﻿.. _authorlabel:

#######
Authors
#######

Major development, maintanance and corresponding author for **custEM**:

    Raphael Rochlitz - raphael.rochlitz@leibniz-liag.de
    
Support for 1D layered-earth EM-field solutions by **COMET**:

    Nico Skibbe - nico.skibbe@leibniz-liag.de
    
Additional support and contact for issues regarding **pyGIMLi**:

    Thomas Günther - thomas.guenther@leibniz-liag.de