# -*- coding: utf-8 -*-
"""
misc
====

Submodules:

- **misc** for general utility functions used in all submodules
- **profiler** for analysing model and performance statistics
- **pyhed_calculations** for calling **pyhed**
- **synthetic_definitions** for defining supporting python functions
- **anomaly_expressions** for defining anomalies with FEniCS, deprecated

################################################################################
"""

try:
    from . misc import mpi_print
    from . misc import logger_print
    from . misc import dump_csr
    from . misc import run_serial
    from . misc import run_multiple_serial
    from . misc import check_if_model_exists
    from . misc import check_approach_and_file_format
    from . misc import specify_td_export_strings
    from . misc import read_paths
    from . misc import root_write
    from . misc import write_h5
    from . misc import read_h5
    from . misc import petsc_transfer_function
    from . misc import make_directories
    from . misc import resort_coordinates
    from . misc import smape
    from . misc import Ground
    from . misc import Air
    from . misc import DirichletBoundary
    from . misc import get_coordinates
    from . misc import block_print
    from . misc import enable_print
except ImportError as err:
    print('Warning, some utility functions could not be imported '
          'from ths "misc" module!\n see:\n')
    print(err)
    print('\n ... "proceeding anyway" ...' )


from . profiler import max_mem
#from . profiler import current_mem
from . profiler import export_config_file
from . profiler import export_resource_file
from . profiler import release_memory
from . profiler import get_logger

from . synthetic_definitions import *

# THE END
