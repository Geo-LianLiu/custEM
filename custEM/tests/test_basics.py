# -*- coding: utf-8 -*-
"""
@author: Rochlitz.R
"""

from custEM.core import MOD
import dolfin as df
from custEM.misc import make_directories


"""
First test which checks if custEM and FEniCS are properly installed with
all requirements via the conda-forge package.
"""

make_directories('base_test_results', 'meshes', 'E_t')

# %% initialize parameters for the 'lvl_1_test'

mpi_cw = df.MPI.comm_world
mpi_size = df.MPI.size(mpi_cw)

# Uncomment the "df.set_log_level(X)" command to enable FEniCS (dolfin) prints.
# default dolfin log_level is 0, which means NO prints from dolfin
# Avilable log levels:
# ########################################################################### #
#     CRITICAL  = 50, errors that may lead to data corruption and suchlike
#     ERROR     = 40, things that go boom
#     WARNING   = 30, things that may go boom later
#     INFO      = 20, information of general interest
#     PROGRESS  = 16, what's happening (broadly)
#     TRACE     = 13, what's happening (in detail)
#     DBG       = 10  sundry
# ########################################################################### #

# df.set_log_level(20)       # set log level here
ll = 10                      # custEM debug level for debugging

# %% Testing fullspace model on UnitCubeMesh with secondary field approach

M = MOD('test_fullspace_E_s', 'UnitCubeMesh', 'E_s', p=1, debug_level=ll,
        overwrite_results=True, r_dir='base_test_results', m_dir='meshes',
        test_mode=True, file_format='xml')

M.MP.update_model_parameters(omega=1e2)

M.FE.build_var_form(pf_type='fullspace',
                    s_type='hed')
M.solve_main_problem(convert=True)


# %% Testing fullspace model on UnitCubeMesh with total field approach

M = MOD('test_fullspace_E_t', 'UnitCubeMesh', 'E_t', p=1, debug_level=ll,
        overwrite_results=True, r_dir='base_test_results', m_dir='meshes',
        test_mode=True, file_format='xml', dg_interpolation=True)

M.MP.update_model_parameters(omega=1e2)

M.FE.build_var_form(s_type='hed',
                    length=2000.1)
M.solve_main_problem(convert=True)

# %% Testing halfspace model on UnitCubeMesh with total field approach

M = MOD('test_halfspace_E_t', 'UnitCubeMesh', 'E_t', p=1, debug_level=ll,
        overwrite_results=True, r_dir='base_test_results', m_dir='meshes',
        test_mode=True, file_format='h5')

M.MP.update_model_parameters(omega=1e2,
                             sigma_ground=1e-2,
                             sigma_air=1e-6)

M.FE.build_var_form(s_type='hed', length=2000.1)
M.solve_main_problem(convert=True)

# %% Testing halfspace model on UnitCubeMesh with secondary field approach

M = MOD('test_halfspace_E_s', 'UnitCubeMesh', 'E_s', p=1, debug_level=ll,
        overwrite=True, r_dir='base_test_results', m_dir='meshes',
        test_mode=True, file_format='h5')

M.MP.update_model_parameters(omega=1e2,
                             sigma_ground=1e-2,
                             sigma_air=1e-6)

M.FE.build_var_form(s_type='hed', length=2000.1)
M.solve_main_problem(convert=True)


if mpi_size > 1:
    if mpi_cw.Get_rank() == 0:
        print('###########################################################')
        print('###########################################################')
        print('###########################################################')
        print('      Basic test performed successfully in mpi mode!!!')
        print('###########################################################')
        print('###########################################################')
        print('###########################################################')
    else:
        pass

else:
    print('###############################################')
    print('###############################################')
    print('###############################################')
    print('      Basic test performed successfully !!!')
    print('###############################################')
    print('###############################################')
    print('###############################################')
