# -*- coding: utf-8 -*-
"""
@author: Rochlitz.R
"""

from custEM.core import MOD
from custEM.misc import max_mem
from custEM.misc import block_print
from custEM.misc import enable_print
from custEM.misc import run_serial
from custEM.misc import mpi_print as mpp
import dolfin as df
import numpy as np
import os
import shutil


"""
Create simple meshes for testing computation with all
time-domain approached supported by custEM.

Uncomment the **df.set_log_level(df.DEBUG)** command to see what FEniCS prints.
"""

ll = 10                           # custEM debug level for debugging

mpi_cw = df.MPI.comm_world
mpi_size = df.MPI.size(mpi_cw)

if df.MPI.rank(df.MPI.comm_world) == 0:
    try:
        shutil.rmtree('td_test_results')
    except FileNotFoundError:
        pass
else:
    pass

df.MPI.barrier(mpi_cw)

# %% Create Mesh and specify example Rx positions

if not os.path.isfile('meshes/_h5/hed_halfspace_mesh.h5'):
    run_serial('create_test_meshes_from_mpi.py')

rx_origs = np.array([[0., 0., -0.1],
                     [150., 100., -0.1]])

###############################################################################

# %% Testing loop Tx halfspace model with inverse Fourier-trans.-based approach

#     !!! The choses frequencies and transformed results are nonsense !!!     #
#                just chosen to speed up the testing process                  #

# initialize model amd parameters
M = MOD('td_test_E_FT', 'loop_brick_mesh', 'E_FT', p=1, debug_level=ll,
        overwrite_results=True, overwrite_mesh=True, serial_ordering=True,
        m_dir='./meshes', r_dir='./td_test_results')

M.MP.update_model_parameters(sigma_ground=[1e-4, 1e-2],
                             rx=[rx_origs.tolist()])
M.IB.create_path_mesh(rx_origs, path_name='rx')

M.FE.build_var_form(n_log_steps=41, log_t_min=-6, log_t_max=0,
                    frequencies=np.logspace(0, 6, 7),
                    interp_mesh='rx_path')

# solve FE problem and interpolate results
M.solve_main_problem()
M.PP.merge_td_results('rx_path')

###############################################################################

# %% Testing HED halfspace model with implicit Euler approach

for st in ['djdt', 'j']:

    # initialize model amd parameters
    M = MOD('td_test_E_IE', 'hed_halfspace_mesh', 'E_IE', p=1, debug_level=ll,
            overwrite_results=True, overwrite_mesh=True, serial_ordering=True,
            m_dir='./meshes', r_dir='./td_test_results')

    M.MP.update_model_parameters(sigma_ground=1e-2, sigma_air=1e-6)

    M.FE.build_var_form(n_lin_steps=10,
                        n_log_steps=7,
                        source_type=st,
                        log_t_min=-6,
                        log_t_max=-0)

    # solve FE problem and interpolate results
    M.solve_main_problem()

    M.IB.interpolate('rx_path')
    M.PP.merge_td_results('rx_path')

###############################################################################

# %% Testing line Tx layered-earth model with Rational Arnoldi approach

# initialize model amd parameters
M = MOD('td_test_E_RA', 'line_dip_plate_mesh', 'E_RA', p=1, debug_level=ll,
        overwrite_results=True, overwrite_mesh=True, serial_ordering=True,
        m_dir='./meshes', r_dir='./td_test_results')

M.MP.update_model_parameters(sigma_ground=[1e-1, 1e-2, 1e-1],
                              sigma_air=1e-6)

M.FE.build_var_form(n_log_steps=41, log_t_min=-6, log_t_max=-2)

# solve FE problem and interpolate results
M.solve_main_problem()
M.IB.interpolate('rx_path')
M.PP.merge_td_results('rx_path')

###############################################################################

if mpi_size > 1:
    if mpi_cw.Get_rank() == 0:
        print('\n')
        print('###########################################################')
        print('###########################################################')
        print('###########################################################')
        print('   TD approach test performed successfully in mpi mode!!!   ')
        print('###########################################################')
        print('###########################################################')
        print('###########################################################')
    else:
        pass
else:
    print('\n')
    print('###############################################')
    print('###############################################')
    print('###############################################')
    print('   TD approach test performed successfully!!!   ')
    print('###############################################')
    print('###############################################')
    print('###############################################')
