# -*- coding: utf-8 -*-

"""
@author: Rochlitz.R
"""

import sys
import os

sys.path.append(os.path.dirname(__file__))

from . import core
from . import fem
from . import inv
from . import post
from . import misc
from . import meshgen
from . import tests
import sys
import os

def run_tests():

    """
    Starting test functions for conda builds.
    """

    import custEM
    testdir = os.path.dirname(__file__)
    print(testdir)
    os.chmod(testdir + '/custEM/tests/run_all_tests.sh', 0o444)
    os.system('bash ' + testdir + '/custEM/tests/run_all_tests.sh')


# conda test try except to prevent NotADirectory Error
try:
    with open(os.path.dirname(__file__) + '/VERSION.rst', 'r') as v_file:
        version = v_file.readline()[:7]
        release_date = v_file.readline()[:10]

except NotADirectoryError:
    version = None
    release_date = None

__version__ = version 
__release__ = release_date