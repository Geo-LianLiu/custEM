custEM.meshgen package
======================

Submodules
----------

custEM.meshgen.bathy\_tools module
----------------------------------

.. automodule:: custEM.meshgen.bathy_tools
   :members:
   :undoc-members:
   :show-inheritance:

custEM.meshgen.dem\_interpolator module
---------------------------------------

.. automodule:: custEM.meshgen.dem_interpolator
   :members:
   :undoc-members:
   :show-inheritance:

custEM.meshgen.invmesh\_tools module
------------------------------------

.. automodule:: custEM.meshgen.invmesh_tools
   :members:
   :undoc-members:
   :show-inheritance:

custEM.meshgen.mesh\_convert module
-----------------------------------

.. automodule:: custEM.meshgen.mesh_convert
   :members:
   :undoc-members:
   :show-inheritance:

custEM.meshgen.meshgen\_tools module
------------------------------------

.. automodule:: custEM.meshgen.meshgen_tools
   :members:
   :undoc-members:
   :show-inheritance:

custEM.meshgen.meshgen\_utils module
------------------------------------

.. automodule:: custEM.meshgen.meshgen_utils
   :members:
   :undoc-members:
   :show-inheritance:

custEM.meshgen.vtk\_convert module
----------------------------------

.. automodule:: custEM.meshgen.vtk_convert
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: custEM.meshgen
   :members:
   :undoc-members:
   :show-inheritance:
